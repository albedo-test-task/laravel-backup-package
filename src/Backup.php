<?php

// packages/simple-backup/backupdb/src/Backup.php
namespace Kustov\Backupdb;

use Illuminate\{Support\Facades\DB};

class Backup
{
    /**
     * Use:
     * use Kustov\Backupdb\Backup;
     *
     * $objDump = new Backup();
     * $objDump->make();
     *
     * @return void
     */
    public function make(): void
    {
        if(!env('DUMP_MAX_FILE_COUNTS')) {
            print "The maximum number of dumps to save is not specified. Define DUMP_MAX_FILE_COUNTS in .env \n\n";
            die();
        }

        $filesPath = storage_path() . "/app/db-backup/";
        $backupFles = scandir($filesPath);
        $backupFles = array_values(array_diff($backupFles, [".", ".."]));
        $countBackupFiles = count($backupFles);

        if($countBackupFiles >= env('DUMP_MAX_FILE_COUNTS')) {
            $countToDelete = $countBackupFiles - env('DUMP_MAX_FILE_COUNTS');

            for ($i=0; $i<=$countToDelete; $i++) {
                exec('rm ' . $filesPath . $backupFles[$i]);
            }
        }


        $dbName = "Tables_in_" . env('DB_DATABASE');
        $tables = DB::select('SHOW TABLES');
        $tables = json_decode(json_encode($tables), true);
        $backupFileName = env('DB_DATABASE', "DB_DATABASE") . '_backup_' . time() . '.sql';
        $filePath = storage_path() . "/app/db-backup/" . $backupFileName;
        $file = fopen($filePath, 'w+');

        foreach ($tables as $table) {
            // Table structure
            $res = DB::select('SHOW CREATE TABLE ' . $table[$dbName]);
            $res = array_values(json_decode(json_encode($res[0]), true));
            $tableName = $res[0];
            $resSql = "";
            $resSql .= "--\n-- Table structure for table `" . $res[0] . "`\n-- \n\n";
            $resSql .= "DROP TABLE IF EXISTS `" . $res[0] . "`; \n\n";
            $resSql .= $res[1] . "; \n\n";
            $resSql .= "--\n-- Dumping data for table `" . $res[0] . "`\n-- \n\n";

            fwrite($file, $resSql);

            // Table data
            $column = DB::select(
                "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" . $tableName . "' AND EXTRA = 'auto_increment'"
            );
            $columnsArray = [];
            $incrementColumn = json_decode(json_encode($column[0]), true);
            $incrementColumn = $incrementColumn['COLUMN_NAME'];

            DB::table($tableName)->orderBy($incrementColumn)
                ->chunk(20000, function ($lines) use (&$file, &$columnsArray, &$tableName) {
                    $linesArray = [];

                    foreach ($lines as $line) {
                        $line = json_decode(json_encode($line), true);
                        $fields = [];

                        foreach ($line as $key => $field) {
                            if (gettype($field) == "string") {
                                $field = "'$field'";
                            }

                            $fields[] = $field;
                            if (!array_key_exists($key, $columnsArray)) $columnsArray[$key] = "`$key`";
                        }

                        $lineString = implode(",", $fields);
                        $linesArray[] = "($lineString)";
                    }

                    $columnsList = "INSERT INTO `" . $tableName . "` (" . implode(",", $columnsArray) . ") VALUES ";
                    $resSql = $columnsList . implode(",", $linesArray) . "; \n";
                    fwrite($file, $resSql);
                });
        }

        fclose($file);
    }
}
